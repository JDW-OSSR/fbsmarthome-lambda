﻿namespace FBSmartHome.GoogleAssistant.Payloads.Response
{
    using FBSmartHome.GoogleAssistant.Types;
    using Newtonsoft.Json;

    /// <summary>
    /// Error payload for response
    /// </summary>
    public class ActionErrorPayload : Payload
    {
        /// <summary>
        /// String. Optional. For systematic errors on SYNC.
        /// </summary>
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; } = ErrorCodes.UNKNOWNERROR;

        /// <summary>
        /// String. Optional. Detailed error which will never be presented to users but may be logged or 
        /// used during development.
        /// </summary>
        [JsonProperty("debugString", NullValueHandling = NullValueHandling.Ignore)]
        public string DebugString { get; set; }
    }
}
