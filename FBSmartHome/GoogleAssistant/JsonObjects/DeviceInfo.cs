﻿namespace FBSmartHome.GoogleAssistant.JsonObjects
{
    using Newtonsoft.Json;

    public class DeviceInfo
    {
        /// <summary>
        /// String. Especially useful when the partner is a hub for other devices. Google may 
        /// provide a standard list of manufacturers here so that e.g. TP-Link and Smartthings 
        /// both describe 'osram' the same way.
        /// </summary>
        [JsonProperty("manufacturer")]
        public string Manufacturer { get; set; }

        /// <summary>
        /// String. The model or SKU identifier of the particular device.
        /// </summary>
        [JsonProperty("model")]
        public string Model { get; set; }

        /// <summary>
        /// String. Specific version number attached to the hardware if available.
        /// </summary>
        [JsonProperty("hwVersion")]
        public string HwVersion { get; set; }

        /// <summary>
        /// String. Specific version number attached to the software/firmware, if available.
        /// </summary>
        [JsonProperty("swVersion")]
        public string SwVersion { get; set; }
    }
}
