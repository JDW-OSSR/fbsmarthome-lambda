﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class Modes
    {
        /// <summary>
        /// Heating/Cooling activity disabled.
        /// </summary>
        public const string OFF = "off";

        /// <summary>
        /// If the device supports heating.
        /// </summary>
        public const string HEAT = "heat";

        /// <summary>
        /// If the device supports cooling.
        /// </summary>
        public const string COOL = "cool";

        /// <summary>
        /// If off, on restores the previous mode of the device.
        /// </summary>
        public const string ON = "on";

        /// <summary>
        /// If the device supports maintaining heating/cooling to target a range.
        /// </summary>
        public const string HEATCOOL = "heatcool";

        /// <summary>
        /// If the device supports an "automatic" mode where the temperature is set based on a schedule, 
        /// learned behavior, AI, or some other related mechanism.
        /// </summary>
        public const string AUTO = "auto";

        /// <summary>
        /// If the device supports a mode where only the fan is on (not fan and another mode like cool.)
        /// </summary>
        public const string FANONLY = "fan-only";

        /// <summary>
        /// If the device supports a purifying mode.
        /// </summary>
        public const string PURIFIER = "purifier";

        /// <summary>
        /// If the device supports an "eco" (that is, energy-saving) mode.
        /// </summary>
        public const string ECO = "eco";

        /// <summary>
        /// If the device supports a dry mode
        /// </summary>
        public const string DRY = "dry";
    }
}
