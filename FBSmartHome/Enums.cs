﻿namespace FBSmartHome
{
    // DeviceClass to specify capabilities of each found device
    public enum DeviceClass
    {
        None = 0,
        AlarmSensor = 4,
        Thermostat = 6,
        Energymeter = 7,
        TemperatureSensor = 8,
        PowerSwitch = 9,
        DECTRepeater = 10,
        Template = 99
    }

    // Switch mode
    public enum Mode
    {
        None,
        Auto,
        Manual
    }

    // Temperatures as defined by thermostat
    public enum Temperature
    {
        MINIMUM_VALUE = 8,
        MAXIMUM_VALUE = 28,
        NOCHANGE = 252,
        OFF = 253,
        ON = 254,
        ECO = 255,
        COMFORT = 256
    }

    // Connection
    public enum ConnectionNumber
    {
        DEFAULT = 1
    }

    // Connection state
    public enum ConnectionState
    {
        OK,
        TIMEOUT,
        UNREACHABLE,
        NOT_AUTHORIZED,
        BLOCKED,
        FAILED,
        EXCEPTION,
        UNKNOWN,
        NOT_CONFIGURED
    }

    public enum Database
    {
        TOKEN = 1,
        LOCKS = 2,
        CUSTOMERS = 3
    }
}