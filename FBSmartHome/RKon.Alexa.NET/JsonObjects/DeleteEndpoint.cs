﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// Endpoints for Discoveryresponse
    /// </summary>
    public class DeleteEndpoint
    {
        /// <summary>
        /// A device identifier. The identifier must be unique across all devices owned by an end user within the domain for the skill. In addition, the identifier needs to be consistent across multiple discovery requests for the same device. The identifier cannot exceed 256 characters.
        /// </summary>
        [JsonProperty("endpointId")]
        [JsonRequired]
        public string EndpointID { get; set; }

        /// <summary>
        /// Initialise ResponseEndpoint
        /// </summary>
        /// <param name="id"></param>
        /// <param name="manufacturer"></param>
        /// <param name="friendlyName"></param>
        /// <param name="description"></param>
        /// <param name="cookies"></param>
        /// <param name="displayCategories"></param>
        /// <param name="capabilities"></param>
        public DeleteEndpoint(string id)
        {
            EndpointID = id;
        }
    }
}
