﻿namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Indicates the temperature scale for the temperature value
    /// </summary>
    public enum Scale
    {
        /// <summary>
        /// °C
        /// </summary>
        CELSIUS,

        /// <summary>
        /// °F
        /// </summary>
        FAHRENHEIT,

        /// <summary>
        /// °K
        /// </summary>
        KELVIN
    }
}
