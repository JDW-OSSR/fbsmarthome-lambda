﻿
namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Types of Capability
    /// </summary>
    public enum CapabilitiyTypes
    {
        /// <summary>
        /// Only supported Capability at the moment
        /// </summary>
        AlexaInterface
    }
}
