﻿
namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Possible error types
    /// </summary>
    public enum ConnectionType
    {
        /// <summary>
        /// TCP_IP
        /// </summary>
        TCP_IP,

        /// <summary>
        /// ZIGBEE
        /// </summary>
        ZIGBEE,

        /// <summary>
        /// ZWAVE
        /// </summary>
        ZWAVE,

        /// <summary>
        /// UNKNOWN
        /// </summary>
        UNKNOWN
    }
}
