﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.JsonObjects;

namespace RKon.Alexa.NET46.Payloads
{
    /// <summary>
    /// Payload for AcceptGrant requests.
    /// </summary>
    public class AcceptGrantRequestPayload : Payload
    {
        /// <summary>
        /// The grant
        /// </summary>
        [JsonProperty("grant")]
        public Grant Grant { get; set; }

        /// <summary>
        /// The grantee
        /// </summary>
        [JsonProperty("grantee")]
        public Grantee Grantee { get; set; }
    }
}
