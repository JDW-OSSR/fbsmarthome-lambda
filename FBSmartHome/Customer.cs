﻿namespace FBSmartHome
{
    using System;
    using System.Collections.Generic;

    // Customer class to save in Database. Holds all neccessary information for the
    // skill to connect
    public class Customer
    {
        public string CustomerId { get; set; } = string.Empty;

        public string AccessToken { get; set; } = string.Empty;

        public string RefreshToken { get; set; } = string.Empty;

        public DateTime TokenValidity { get; set; } = DateTime.MinValue;

        public string ApiKey { get; set; } = string.Empty;

        public bool IsGoogleAssistant { get; set; } = false;

        public string UserRegion { get; set; } = string.Empty;

        public List<Device> DeviceList { get; set; } = new List<Device>();

        public List<Connection> ConnectionList { get; set; } = new List<Connection>();

        public long TTL { get; set; } = ((DateTimeOffset)DateTime.UtcNow.AddDays(14)).ToUnixTimeSeconds();

        public Connection GetConnection(ConnectionNumber connectionNumber)
        {
            // Try to find connection
            Connection connectionItem = ConnectionList.Find(connection => connection.Number == connectionNumber);

            // If connection was found, return connection
            if (connectionItem != null)
            {
                return connectionItem;
            }

            // Else try to find default connection
            connectionItem = ConnectionList.Find(connection => connection.Number == ConnectionNumber.DEFAULT);

            // If connection was found, return connection, else return new connection
            return connectionItem ?? new Connection();
        }

        public bool IsConnection(ConnectionNumber connectionNumber)
        {
            foreach (Connection connection in ConnectionList)
            {
                if (connection.Number == connectionNumber)
                {
                    return true;
                }
            }
            return false;
        }

        public void RemoveDevices(ConnectionNumber connectionNumber)
        {
            List<Device> newDeviceList = new List<Device>();
            foreach (Device device in DeviceList)
            {
                if (device.ConnectionNumber != connectionNumber)
                {
                    newDeviceList.Add(device);
                }
            }
            DeviceList = newDeviceList;
        }

        public void RemoveDevice(Device removeDevice)
        {
            List<Device> newDeviceList = new List<Device>();
            foreach (Device device in DeviceList)
            {
                if (device.Identifier != removeDevice.Identifier)
                {
                    newDeviceList.Add(device);
                }
            }
            DeviceList = newDeviceList;
        }
    }
}