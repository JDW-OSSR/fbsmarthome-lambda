﻿namespace FBSmartHome
{
    using Amazon;
    using Amazon.DynamoDBv2;
    using Amazon.DynamoDBv2.DataModel;
    using Amazon.DynamoDBv2.Model;
    using System;
    using System.Threading.Tasks;

    public sealed class DynamoDB
    {
        public static readonly DynamoDB Instance = new DynamoDB();

        private readonly AmazonDynamoDBClient client;
        private readonly DynamoDBContext context;

        static DynamoDB()
        {
        }

        private DynamoDB()
        {
            AmazonDynamoDBConfig clientConfig = new AmazonDynamoDBConfig
            {
                RegionEndpoint = RegionEndpoint.EUWest1,
            };
            client = new AmazonDynamoDBClient(clientConfig);
            context = new DynamoDBContext(client);
        }

        // Get customer from DB using customer id as hash key
        public async Task<Customer> GetCustomerAsync(string customerId)
        {
            try
            {
                Customer customer = await context.LoadAsync<Customer>(customerId).ConfigureAwait(false);

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|dynamodb.read|#id", DateTimeOffset.Now.ToUnixTimeSeconds(), 1);

                if (customer == null)
                {
                    Logger.Instance.Debug("Customer not found, creating new customer");
                    customer = new Customer();
                }

                return customer;
            }
            catch (Exception ex)
            {
                Logger.Instance.Debug("Exception on accessing database: {0}", ex.Message);
                return null;
            }
        }

        // Save customer to db
        public async Task<bool> SaveCustomerAsync(Customer customer)
        {
            try
            {
                await context.SaveAsync<Customer>(customer).ConfigureAwait(false);
                Logger.Instance.Debug("Customer saved.");

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|dynamodb.write|", DateTimeOffset.Now.ToUnixTimeSeconds(), 1);

                return true;
            }
            catch (ResourceNotFoundException)
            {
                Logger.Instance.Debug("DynamoDB Table was not found!");
                return false;
            }
        }
    }
}